// Display/Show Posts
fetch("https://jsonplaceholder.typicode.com/posts")
.then(result => result.json())
.then(data => showPost(data));

const showPost = (posts) => {

	let postEntries = ""

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries
}

// Create/Add Post
document.querySelector("#form-add-post").addEventListener("submit", (event) => {

	event.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then(result => result.json())
	.then(data => {
		console.log(data)
		alert("Successfully Added!")

		// this clears the input fields after the data is successfully added
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	})
})

// Edit Post
const editPost = (postId) => {

	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
	document.querySelector("#txt-edit-id").value = postId;
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

// Update/Edit Post
document.querySelector("#form-edit-post").addEventListener("submit", (event) => {

	event.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then(result => result.json())
	.then(data => {
		console.log(data);
		alert("Successfully Updated!")

		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;
		document.querySelector("#btn-submit-update").setAttribute("disabled", true);
	})
})

// Delete Post
const deletePost = (postId) => {

	fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
		method: "DELETE"
	})
	.then(data => {
		console.log(data);
		alert(`Post ${postId} Successfully Deleted!`)	
	});

	document.querySelector(`#post-${postId}`).remove();

};
